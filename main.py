#!/usr/bin/env python3

# import required libraries
import RPi.GPIO as GPIO
import time
from threading import Thread

# used to disable each system for debugging
buzz_enabled = True
beep_enabled = True

# set constants for each port on the pi
TRIG = 11
ECHO = 12
BUZZER = 13
VIBRATE = 36
MOTION = 40

def setup():
    # runs once when turning on
    # setup the board and set each pinmode
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(TRIG, GPIO.OUT)
    GPIO.setup(ECHO, GPIO.IN)
    GPIO.setup(BUZZER, GPIO.OUT)
    GPIO.output(BUZZER, GPIO.HIGH)
    GPIO.setup(VIBRATE, GPIO.OUT)
    GPIO.setup(MOTION, GPIO.IN)

    # start a new thread for the haptic/ultrasound so it can run 
    # concurrently with the motion sensor
    if buzz_enabled:
        b = Buzz()
        b.start()

# calculate the distance from the ultrasonic sensor
def distance():
    # trigger the sensor
	GPIO.output(TRIG, 0)
	time.sleep(0.000002)

	GPIO.output(TRIG, 1)
	time.sleep(0.00001)
	GPIO.output(TRIG, 0)
    # sensor has been triggered

	# wait for a response and record it
	while GPIO.input(ECHO) == 0:
		a = 0
	time1 = time.time()
	while GPIO.input(ECHO) == 1:
		a = 1
	time2 = time.time()

    # get the time between the on/off pulse from the sensor
	during = time2 - time1
    # use the time to convert to distance
	return during * 340 / 2 * 100

# get the rate of buzzing for the haptic device
def getRate():
    # calculate distance
    d = distance()
    # convert to meters
    m = d/100
    # max out the time it can return at 1.5
    return min(m,1.5)

# runs constantly while turned on
def loop():
    # check if it's enabled
    if beep_enabled:
        # wait until motion sensor detects motion
        waitForMotion()
        time.sleep(0.1)
        # make sound
        beep()
        # don't trigger again until 2 seconds after movement stops
        waitForNoMotion()
        time.sleep(2)

def waitForMotion():
    # loop (wait) forever until the MOTION input = 1
    while True:
        i = GPIO.input(MOTION)
        if i == 1:
            break

def waitForNoMotion():
    # loop (wait) forever until the MOTION input = 0
    while True:
        i = GPIO.input(MOTION)
        if i == 0:
            break

# called when turning off
def destroy():
    # make sure to turn off the buzzer
    GPIO.output(BUZZER, GPIO.HIGH)
    GPIO.cleanup()

# makes a beep sound
def beep():
    # beep twice
    for _ in range(2):
        # turn on and off the buzzer (sound device) pin with a small delay
        GPIO.output(BUZZER, GPIO.LOW)
        time.sleep(0.1)
        GPIO.output(BUZZER, GPIO.HIGH)
        time.sleep(0.1)

# buzzes the haptic device with delay = x * 0.7 (chosen as a random constant because it feels the best)
def buzz(x):
    # print x for debug
    print(x)
    # if the distance (comes from getRate()) is over 1 meter, don't buzz at all
    if x > 1:
        return
    # set it to high and then to low and wait x*0.7 seconds
    GPIO.output(VIBRATE,GPIO.HIGH)
    time.sleep(0.2)
    GPIO.output(VIBRATE, GPIO.LOW)
    time.sleep(x*0.7)

# the thread class so the distance sensor can run concurrently with the motion sensor
class Buzz(Thread):
    # initialize the thread
    def __init__(self):
        super(Buzz, self).__init__()

    # this method is called on another thread
    def run(self):
        # loop forever (on second thread)
        while True:
            # constantly get the rate and buzz with the returned value as delay
            buzz(getRate())

            
# set up the program
if __name__ == "__main__":
    # run setup once
	setup()
	try:
        # run loop forever (controls the motion sensor)
		while True:
			loop()
    # call destroy() on CTRL+C
	except KeyboardInterrupt:
		destroy()

